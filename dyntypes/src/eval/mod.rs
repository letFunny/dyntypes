
use std::cell::{RefCell};
use std::collections::BTreeMap;
use std::fmt::{Debug, Formatter};
use std::ops::Deref;

use std::rc::Rc;
use once_cell::sync::Lazy;

use crate::Environment;
use crate::eval::builtin::BUILTINS;
use crate::lang::{Block, Expression, Function, Identifier, SpecialForm, Statement, Value};

pub mod builtin;

// TODO: can we omit the expression in function if there is a return?
// TODO: interning of identifiers and (questionable ->) remove interning of types.
// TODO: in memories, write about performance of having the expressiveness of combining objects at
// TODO: Check that types in function signatures are defined
// runtime. Maybe touch on dynamic => less performant, for instance javascript optimizations revolve
// around making fields static.

#[derive(Debug, Clone)]
pub struct Closure {
    env: Rc<RefCell<Environment<IValue>>>,
    fun: Rc<Function>,
}

impl Environment<IValue> {
    pub fn lookup_field(&mut self, id: &Identifier) -> Option<IValue> {
        // We try to recursively resolve identifiers with dots as <struct1>.<struct2>.<structN>.field
        // Note: if there is no dot, main_id is directly the variable: "abc".split(".") == ["abc"]
        let mut it = id.split('.');
        // It is never gonna fail because it is the first one.
        let main_id = it.next().unwrap();

        // Get the type to start traversing for fields.
        let mut v = self.lookup_value(&main_id.to_string())?;
        for field in it {
            // We are now looking at main.{field} where {field} can have nested fields.
            // First of all we have to make sure it is a derived type.
            if let IValue::Derived(ref fs) = v {
                // Then we have to extract the "turbofish" name in {field} i.e. a::A that is
                // field a of struct A.
                let mut it = field.split("::");
                let field_alone = it.next().unwrap();
                let facet = it.next().unwrap_or_else(|| -> &str {
                    // If only a field has been supplied, there is only one facet with such field.
                    for (f, fields) in fs {
                        if fields.contains_key(field_alone) {
                            return f
                        }
                    }
                    panic!()
                });

                self.bind_value(".self".to_string(), &v);
                // We can unwrap because it has been type checked.
                v = fs.get(facet).unwrap().get(field_alone).cloned().unwrap();
            } else {
                panic!()
            }
        }
        Some(v)
    }
    pub fn bind_field(&mut self, id: Identifier, val: &IValue) {
        // TODO optimize control flow to avoid peeking iterator.
        // We try to recursively resolve identifiers with dots as <struct1>.<struct2>.<structN>.field
        // Note: if there is no dot, main_id is directly the variable: "abc".split(".") == ["abc"]
        let mut it = id.split('.').peekable();
        // It is never gonna fail because it is the first one.
        let main_id = it.next().unwrap();

        // If we are asking for a field of (nested) structs, add it by traversing them.
        if it.peek().is_some() {
            // We get the struct.
            let mut v = self.env.get_mut(main_id);
            for field in it {
                if let Some(IValue::Derived(ref mut fs)) = v {
                    // Then we have to extract the "turbofish" name in {field} i.e. a::A that is
                    // field a of struct A.
                    let mut it = field.split("::").peekable();
                    let field_alone = it.next().unwrap();
                    let facet: String = it.next().map(|s| s.to_string()).unwrap_or_else(|| -> String {
                        // If only a field has been supplied, there is only one facet with such field.
                        for (f, fields) in fs.deref() {
                            if fields.contains_key(field_alone) {
                                return f.to_string()
                            }
                        }
                        panic!()
                    });
                    // If it is the last one, add it to the map otherwise keep traversing.
                    if it.peek().is_some() {
                        v = fs.get_mut(facet.as_str()).unwrap().get_mut(field_alone);
                    } else {
                        fs.get_mut(facet.as_str()).unwrap().insert(Identifier::from(field_alone), val.clone());
                        return;
                    }
                } else {
                    panic!()
                }
            }
        } else {
            // If it is not a field but a simple binding add it directly to the scope.
            self.env.insert(id, val.clone());
        }
    }
}

type BuiltIn = fn(&[IValue]) -> IValue;
#[derive(Clone)]
pub enum IValue {
    // This is an auxiliary type that gets bubbled up from the function body to mark an early
    // return. (TODO In the book he uses exceptions, we use this)
    Return(Box<IValue>),
    // We store each identification name->facet, for each facet we also store field->value.
    Derived(BTreeMap<Identifier, BTreeMap<Identifier, IValue>>),
    Closure(Closure),
    BuiltIn(BuiltIn),
    Number(i64),
    String(String),
    Boolean(bool),
    SpecialForm(SpecialForm),
    Nil,
}

impl IValue {
    fn is_boolean(&self) -> bool {
        return self.get_boolean().is_some()
    }
    fn is_string(&self) -> bool {
        return self.get_string().is_some()
    }
    fn is_number(&self) -> bool {
        return self.get_number().is_some()
    }
    fn get_boolean(&self) -> Option<bool> {
        if let IValue::Boolean(b) = self {
            return Some(*b)
        }
        return None
    }
    fn get_string(&self) -> Option<&str> {
        if let IValue::String(s) = self {
            return Some(s)
        }
        return None
    }
    fn get_number(&self) -> Option<i64> {
        if let IValue::Number(n) = self {
            return Some(*n)
        }
        return None
    }
}

impl std::fmt::Debug for IValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self)
    }
}
impl std::fmt::Display for IValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            IValue::BuiltIn(_)  => {
                write!(f, "<built-int>")
            }
            IValue::Closure(c) => {
                write!(f, "<closure {:?}>", c)
            }
            IValue::Number(n) => {
                write!(f, "{}", n)
            }
            IValue::String(s) => {
                write!(f, "{}", s)
            }
            IValue::Boolean(b) => {
                write!(f, "{}", b)
            }
            IValue::Nil => {
                write!(f, "nil")
            }
            IValue::Return(r) => {
                write!(f, "<return {}>", r)
            }
            IValue::Derived(fs) => {
                write!(f, "<facet {:?}>", fs)
            }
            IValue::SpecialForm(s) => {
                write!(f, "<special form {:?}>", s)
            }
        }
    }
}

pub trait Eval {
    fn eval(&self, env: &Rc<RefCell<Environment<IValue>>>) -> IValue;
}

impl Eval for Block {
    fn eval(&self, env: &Rc<RefCell<Environment<IValue>>>) -> IValue {
        for stmt in &self.stmts {
            if let Statement::Return(expr) = stmt {
                return IValue::Return(Box::new(expr.eval(env)));
            } else {
                // Eval the statement and check for nested returns.
                let val = stmt.eval(env);
                if let IValue::Return(_) = val {
                    return val;
                }
            }
        }
        self.expr.eval(env)
    }
}

impl Eval for Statement {
    fn eval(&self, env: &Rc<RefCell<Environment<IValue>>>) -> IValue {
        match self {
            Statement::Expression(e) => {
                // Statements return empty (): in our language, null except if we are bubbling up a return
                // statement.
                if let i @ IValue::Return(_) = e.eval(env) {
                    return i;
                } else {
                    return IValue::Nil;
                }
            }
            Statement::While { condition, body } => {
                let scope = Environment::from_parent(env);
                while condition.eval(&scope).get_boolean().unwrap_or(false) {
                    // If we get a return, break from the while early.
                    if let i @ IValue::Return(_) = body.eval(&scope) {
                        return i
                    }
                }
            }
            Statement::Binding { var, value, var_type: _ } => {
                let v = value.eval(env);
                env.borrow_mut().bind_value(Identifier::from(var), &v);
            }
            Statement::Assignment {value, var} => {
                let v = value.eval(env);
                env.borrow_mut().bind_field(Identifier::from(var), &v);
            }
            Statement::Return(_) => {
                panic!("Return outside of block!")
            }
            Statement::StructDecl {..} => {
                // We dont have to do anything, the declaration was already used by the type checker.
            }
        };
        IValue::Nil
    }
}

impl Eval for Expression {
    fn eval(&self, env: &Rc<RefCell<Environment<IValue>>>) -> IValue {
        match self {
            Expression::FunctionCall {op, children} => {
                let op_eval = op.as_ref().eval(env);
                // Eval all the args to the function call. We have to evaluate the args in the current
                // environment, NOT in the one captured by the closure.
                let args: Vec<IValue> = children.iter().map(|c| c.eval(&env.clone())).collect();
                match op_eval {
                    IValue::BuiltIn(fun) => {
                        return fun(&args)
                    },
                    IValue::Closure(Closure{env, fun}) => {
                        let scope = Environment::from_parent(&env);
                        let Function{ arg_names, body, ..} = fun.as_ref();
                        for (name, arg) in arg_names.iter().zip(args) {
                            (*scope).borrow_mut().bind_value(name.to_owned(), &arg)
                        }
                        let res = body.eval(&scope);
                        if let IValue::Return(r) = res {
                            *r
                        } else {
                            res
                        }
                    },
                    IValue::SpecialForm(s) => {
                        match s {
                            SpecialForm::At => {
                                match (args.get(0).unwrap(), args.get(1).unwrap()) {
                                    (IValue::Derived(fs1), IValue::Derived(fs2)) => {
                                        let mut fs = fs1.clone();
                                        fs.append(&mut fs2.clone());
                                        IValue::Derived(fs)
                                    }
                                    _ => {
                                        panic!("Attempting to call @ on non derived types!")
                                    }
                                }
                            },
                            SpecialForm::Drop(dfacet) => {
                                // See typechecking for brief discussing into design.
                                let mut obj = env.borrow().lookup_value(&".self".to_string()).unwrap();
                                if let IValue::Derived(ref mut fs) = obj {
                                    fs.remove(dfacet.as_str());
                                } else {
                                    panic!("Calling drop on non derived type!");
                                }
                                return obj;
                            }
                        }
                    },
                    _ => {
                        // It is not a function
                        panic!("Error: expected function.")
                    }
                }
            }
            Expression::Value(v) => {
                match v {
                    Value::Function(fun) => {
                        IValue::Closure(Closure {
                            env: env.clone(),
                            fun: Rc::new(fun.clone())
                        })
                    }
                    Value::Number(n) => {
                        IValue::Number(*n)
                    }
                    Value::String(s) => {
                        IValue::String(s.to_owned())
                    }
                    Value::Identifier(id) => {
                        // If we find an identifier, resolve it. We can unwrap because it has been
                        // typechecked.
                        env.borrow_mut().lookup_field(id).unwrap()
                    }
                    Value::Boolean(b) => {
                        IValue::Boolean(*b)
                    }
                    Value::Nil => {
                        IValue::Nil
                    }
                }
            },
            Expression::Block(b) => {
                let block_env = Environment::from_parent(env);
                let ret = b.as_ref().eval(&block_env);
                ret
            }
            Expression::If { condition, body_f, body_t } => {
                let c = condition.eval(env);
                match c {
                    IValue::Boolean(true) => {
                        return body_t.eval(env)
                    },
                    IValue::Boolean(false) => {
                        return body_f.eval(env)
                    },
                    _ => {
                        panic!("If condition not a boolean!")
                    }
                }
            }
            Expression::Struct {members, name} => {
                let mut fields = BTreeMap::new();
                for (mid, mexp) in members {
                    fields.insert(mid.clone(), mexp.eval(env));
                }
                fields.insert("drop".to_string(), IValue::SpecialForm(SpecialForm::Drop(name.to_owned())));
                let mut facets = BTreeMap::new();
                facets.insert(name.clone(), fields);
                IValue::Derived(facets)
            }
        }
    }
}

pub fn bind_builtin(env: &Rc<RefCell<Environment<IValue>>>, id: Identifier, fun: BuiltIn) {
    env.borrow_mut().bind_value(id, &Rc::new(IValue::BuiltIn(fun)));
}
pub fn eval(program: &impl Eval) {
    let special_forms: Lazy<[(Identifier, IValue); 1]> = Lazy::new(|| {
        [(Identifier::from("@"), IValue::SpecialForm(SpecialForm::At))]
    });

    // Create the global environment.
    let env = Environment::new();
    for (id, bi, _) in BUILTINS.iter() {
        bind_builtin(&env, id.clone(), *bi);
    }
    for (id, val) in special_forms.iter() {
        env.borrow_mut().bind_value(id.clone(), val);
    }

    program.eval(&env);
}