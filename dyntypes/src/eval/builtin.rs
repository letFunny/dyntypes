

use once_cell::sync::Lazy;
use crate::eval::{BuiltIn, IValue};
use crate::lang::{Identifier, Signature};
use crate::typecheck::TYPE_CORRESPONDENCE;

pub static BUILTINS: Lazy<[(Identifier, BuiltIn, Signature); 4]> = Lazy::new(|| {[
    (Identifier::from("+"),
     builtin_sum,
     Signature {
         args: vec![TYPE_CORRESPONDENCE.int.clone(), TYPE_CORRESPONDENCE.int.clone()],
         ret: TYPE_CORRESPONDENCE.int.clone(),
         generics: Vec::with_capacity(0),
     }),
    (Identifier::from("-"),
     builtin_sub,
     Signature {
         args: vec![TYPE_CORRESPONDENCE.int.clone(), TYPE_CORRESPONDENCE.int.clone()],
         ret: TYPE_CORRESPONDENCE.int.clone(),
         generics: Vec::with_capacity(0),
     }),
    (Identifier::from("<"),
     builtin_lt,
     Signature {
         args: vec![TYPE_CORRESPONDENCE.int.clone(), TYPE_CORRESPONDENCE.int.clone()],
         ret: TYPE_CORRESPONDENCE.bool.clone(),
         generics: Vec::with_capacity(0),
     }),
    (Identifier::from("print"),
     builtin_print,
     Signature {
         args: vec![TYPE_CORRESPONDENCE.any.clone()],
         ret: TYPE_CORRESPONDENCE.nil.clone(),
         generics: Vec::with_capacity(0),
     }),
]});

pub fn builtin_sum(args: &[IValue]) -> IValue {
    match (args.get(0), args.get(1)) {
        (Some(IValue::Number(n1)), Some(IValue::Number(n2))) => {
            return IValue::Number(n1 + n2);
        },
        _ => {
            panic!("Error: bad types or bad arity.")
        }
    }
}

pub fn builtin_sub(args: &[IValue]) -> IValue {
    match (args.get(0), args.get(1)) {
        (Some(IValue::Number(n1)), Some(IValue::Number(n2))) => {
            return IValue::Number(n1 - n2);
        },
        _ => {
            panic!("Error: bad types or bad arity.")
        }
    }
}

pub fn builtin_lt(args: &[IValue]) -> IValue {
    match (args.get(0), args.get(1)) {
        (Some(IValue::Number(n1)), Some(IValue::Number(n2))) => {
            return IValue::Boolean(n1 < n2)
        },
        _ => {
            panic!("Error: bad types or bad arity.")
        }
    }
}


pub fn builtin_print(args: &[IValue]) -> IValue {
    for arg in args {
        println!("{}", arg);
    }
    IValue::Nil
}
