use clap::{App, Arg};
use dyntypes::eval::eval;
use dyntypes::{Information, PError};
use dyntypes::lang::{Expression, graph, parse_program};
use dyntypes::typecheck::type_check;


fn parse(input: &str) -> Result<Expression, PError> {
    let chars = input.chars().collect::<Vec<char>>();
    let info = Information::new(&chars);
    let p = parse_program(info);
    match p {
        Ok((i, exp)) => {
            if i.index != input.len() {
                // If we have only parsed part of the program, through error.
                Err(PError {
                    index: i.index,
                    line:  i.line,
                })
            } else {
                // If we have consumed all the inputs and no errors, return the parsed program.
                Ok(exp)
            }
        }
        Err(e) => {
            // If we have an error, preserve it.
            Err(e)
        }
    }
}

fn main() {
    let matches = App::new("dyntypes")
        .about("")
        .arg(Arg::with_name("graph")
            .long("graph")
            .help("Outputs the AST to ast.dot")
            .takes_value(false))
        .arg(Arg::with_name("INPUT")
            .help("Sets the input file to use")
            .required(true)
            .index(1))
        .get_matches();

    let input = match std::fs::read_to_string(matches.value_of("INPUT").unwrap()) {
        Ok(i) => {i}
        Err(e) => {
            eprintln!("Could not read input file: {}", e);
            std::process::exit(-1);
        }
    };
    // First parse the program.
    let program = match parse(&input) {
        Ok(p) => p,
        Err(e) => {
            eprintln!("[PARSE ERROR] {}.", e);
            std::process::exit(1);
        }
    };
    // Graph it if the cli argument is present.
    if matches.is_present("graph") {
        if let Err(e) = graph("ast.dot", &program) {
            eprintln!("Could not create a dot file: {}.", e);
        }
    }
    // Then, type check it.
    if let Err(e) = type_check(&program) {
        eprintln!("[TYPE ERROR] {}.", e);
        std::process::exit(1);
    }
    // Lastly, the actual evaluation.
    eval(&program);
}
