use std::cell::RefCell;
use std::collections::BTreeMap;
use std::fmt::Formatter;

use std::ops::Deref;
use std::rc::Rc;
use once_cell::sync::Lazy;
use crate::{BUILTIN_TYPES, Environment};
use crate::eval::builtin::BUILTINS;
use crate::lang::{Block, Expression, Facet, Function, Identifier, SpecialForm, Statement, Type, Value};

#[derive(Debug, Clone)]
pub struct TError {
    reason: String,
}

impl std::fmt::Display for TError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.reason)
    }
}

pub struct TypeCorrespondence {
    pub bool: Type,
    pub int:  Type,
    pub nil:  Type,
    pub str:  Type,
    pub any:  Type,
}

pub static TYPE_CORRESPONDENCE: Lazy<TypeCorrespondence> = Lazy::new(|| {
    TypeCorrespondence {
        int:  Type::new_primitive("int"),
        bool: Type::new_primitive("bool"),
        nil:  Type::new_primitive("nil"),
        str:  Type::new_primitive("str"),
        any:  Type::new_primitive(""),
    }
});

static SPECIAL_FORMS: Lazy<[(Identifier, Type); 1]> = Lazy::new(|| {
    [(Identifier::from("@"), Type::new_special_form(SpecialForm::At))]
});

impl Environment<Type> {
    pub fn lookup_field(&mut self, id: &Identifier) -> Result<Type, TError> {
        // We try to recursively resolve identifiers with dots as <struct1>.<struct2>.<structN>.field
        // Note: if there is no dot, main_id is directly the variable: "abc".split(".") == ["abc"]
        let mut it = id.split('.');
        // It is never gonna fail because it is the first one.
        let main_id = it.next().unwrap();

        // Get the type for main_id which should be a Derived type to start traversing facets/struct.
        let mut v = self.lookup_value(&main_id.to_string())
            .ok_or(TError {
                reason: format!("Identifier {} not found", main_id)
            })?;
        for field in it {
            // We are now looking at main.{field} where {field} can have nested fields.
            // First of all we have to make sure it is a derived type.
            if let Type::Derived(ref fs) = v {
                // Then we have to extract the "turbofish" name in {field} i.e. a::A that is
                // field a of struct A.
                let mut it = field.split("::");
                let field_alone = it.next().unwrap();
                // let facet = it.next().unwrap_or_else(|| panic!("Attempting to access a field without specifying facet, {}!", id));
                let facet_opt = it.next();
                let facet = match facet_opt {
                    Some(facet) => {
                        facet
                    },
                    None => {
                        // The only possibility is that there is only one facet with such field.
                        let facet_vec: Vec<&Facet> = fs.iter().filter(|f| {
                            if let Some(Type::Struct(ref fs)) = self.lookup_value(f) {
                                fs.contains_key(field_alone)
                            } else {
                                false
                            }
                        }).collect();
                        if facet_vec.len() > 1 {
                            return Err(TError {
                                reason: format!("Attempting to access a field without specifying facet, {}", id)
                            });
                        } else if facet_vec.len() == 0 {
                            return Err(TError {
                                reason: format!("Field {} does not exist", id)
                            });
                        }
                        facet_vec.get(0).unwrap().to_owned()
                    }
                };
                // If we have asked for a facet that our object does not have, error.
                if !fs.contains(facet) {
                    return Err(TError {
                        reason: format!("Field {} does not exist", id)
                    });
                }
                // Get the type inside the struct named <facet>.
                if let Some(Type::Struct(ref fs)) = self.lookup_value(&facet.to_owned()) {
                    // We can unwrap because we have check that it exists above.
                    if let Some(t) = fs.get(field_alone) {
                        self.bind_value(".self".to_string(), &v);
                        v = t.clone()
                    } else {
                        return Err(TError {
                            reason: format!("Field {} does not exist", id)
                        });
                    }
                } else {
                    return Err(TError {
                        reason: format!("Identifier {} not found", id)
                    });
                }
            } else {
                return Err(TError {
                    reason: format!("Attempting to access field of non-derived type: {}", id)
                });
            }
        }
        Ok(v)
    }
}

fn format_type_list(ts: &Vec<Type>) -> String {
    let mut fmt_arglist = String::new();
    // First argument if any.
    for arg in ts.iter().take(1) {
        fmt_arglist = fmt_arglist + format!("{}", arg).as_str();
    }
    // Rest of them
    for arg in ts.iter().skip(1) {
        fmt_arglist = fmt_arglist + ", " + format!("{}", arg).as_str();
    }
    fmt_arglist
}

/// Returns true if type1 < type2.
pub fn is_subtype(type1: &Type, type2: &Type) -> bool {
    if type2 == &TYPE_CORRESPONDENCE.any {
        return true;
    }
    type1 == type2
    // match (type1, type2) {
    //     (Type::Derived(fs1), Type::Derived(fs2)) => {
    //         fs2.is_subset(fs1)
    //     },
    //     _ => {
    //         type1 == type2
    //     }
    // }
}

pub fn is_type_defined(ty: &Type, scope: &TEnv) -> Result<(), TError> {
    match ty {
        Type::Primitive(f) => {
            // If it is not a builtin type, we try to resolve it and, if it is not found, error.
            if !BUILTIN_TYPES.contains(&f.as_str()) && scope.borrow().lookup_value(f).is_none() {
                return Err(TError {
                    reason: format!("Type {} is not defined", f)
                });
            }
        }
        Type::Derived(fs) => {
            for f in fs {
                scope.borrow().lookup_value(f).ok_or_else(|| TError {
                    reason: format!("Type {} is not defined", f)
                })?;
            }
        }
        _ => {
            // We should not encounter any of the other types here. That is
            // an error on the compiler.
            panic!("We should not encounter a type here that is not primitive \
                                    or derived")
        }
    }
    Ok(())
}

type TEnv = Rc<RefCell<Environment<Type>>>;
pub trait TypeCheck {
    fn type_check(&self, env: &TEnv) -> Result<Type, TError>;
}

impl TypeCheck for Block {
    fn type_check(&self, env: &TEnv) -> Result<Type, TError> {
        for stmt in &self.stmts {
            stmt.type_check(env)?;
        }
        self.expr.type_check(env)
    }
}

impl TypeCheck for Statement {
    fn type_check(&self, env: &TEnv) -> Result<Type, TError> {
        match self {
            Statement::Binding { var, value, var_type } => {
                // When creating a variable, check that it does not clash with builtin types.
                if BUILTIN_TYPES.contains(&var.as_str()) {
                    return Err(TError {
                        reason: format!("Reserved variable name: {}", var)
                    });
                }
                let v = value.type_check(env)?;
                if let Some(typ) = var_type {
                    if !is_subtype(&v, typ) {
                        return Err(TError {
                            reason: format!("Trying to assign {} to variable {} of type {}", v, var, typ)
                        });
                    }
                }
                // We dont have to check if it exists because we always shadow variables.
                // It is important that we return v in the case of functions because v has been
                // type-erased and the type hint has not.
                env.borrow_mut().bind_value(Identifier::from(var), &v);
            }
            Statement::Assignment {value, var} => {
                let v = value.type_check(env)?;
                if let Some(last_field) = var.split(".").last() {
                    if last_field == "drop" {
                        return Err(TError {
                            reason: "Field 'drop' is immutable".to_string()
                        });
                    }
                }
                let type_var = env.borrow_mut().lookup_field(var)?;
                if !is_subtype(&v, &type_var) {
                    return Err(TError {
                        reason: format!("Trying to assign {} to variable {} of type {}", v, var, type_var)
                    });
                }
            }
            Statement::Expression(e) => {
                e.type_check(env)?;
            }
            Statement::While { condition, body } => {
                if condition.type_check(env)? != TYPE_CORRESPONDENCE.bool {
                    return Err(TError {
                        reason: "While condition has to be a primitive boolean".to_string()
                    });
                }
                body.type_check(env)?;
            }
            Statement::Return(e) => {
                let ffun = env.borrow().lookup_value(&Identifier::from("."))
                    .ok_or(TError {
                        reason: "Return outside of a function".to_string()
                    })?;
                if let Type::Function(ref sig, _) = ffun {
                    let ret = e.type_check(env)?;
                    if ret != sig.ret {
                        return Err(TError {
                            reason: format!("Return value {} does not match signature {}", ret, sig.ret)
                        });
                    }
                    return Ok(ret);
                } else {
                    panic!("Error: expected '.' to be bound to a function but it is bounded to \
                    something else");
                }
            }
            Statement::StructDecl {name, members} => {
                let mut fields = BTreeMap::new();
                for (mid, mty) in members {
                    fields.insert(mid.clone(), mty.clone());
                }
                fields.insert("drop".to_string(), Type::new_special_form(SpecialForm::Drop(name.to_owned())));
                let ty = Type::new_struct(fields);
                env.borrow_mut().bind_value(name.clone(), &ty);
            }
        }
        // All the statements return nil except if it is a bubbled up return statement.
        Ok(TYPE_CORRESPONDENCE.nil.clone())
    }
}

impl TypeCheck for Expression {
    fn type_check(&self, env: &TEnv) -> Result<Type, TError> {
        match self {
            Expression::FunctionCall {op, children} => {
                let op_type = op.as_ref().type_check(env)?;
                // Eval all the args to the function call. We have to evaluate the args in the current
                // environment, NOT in the one captured by the closure.
                let args: Vec<Type> = children.iter().map(|c| c.type_check(&env.clone())).collect::<Result<Vec<Type>, TError>>()?;
                match op_type {
                    Type::Function(ref s, _) => {
                        // Check that arity is correct, then check that all the args are subtypes of the
                        // signature. (Functions are contravariant in their argument types).
                        if s.args.len() == args.len() &&
                            args.iter().zip(s.args.iter()).all(|(a, sa)| is_subtype(a, sa)) {
                            Ok(s.ret.clone())
                        } else {
                            return Err(TError {
                                reason: format!("Expected arg types to be: ({}), got: {}", format_type_list(&s.args), format_type_list(&args))
                            });
                        }
                    },
                    Type::SpecialForm(s) => {
                        match s {
                            SpecialForm::At => {
                                match (args.get(0).unwrap().deref().deref(), args.get(1).unwrap().deref().deref()) {
                                    (Type::Derived(ref ty1), Type::Derived(ref ty2)) => {
                                        if !ty1.is_disjoint(ty2) {
                                            // TODO test of this
                                            return Err(TError {
                                                reason: format!("Attempting to add more than one facet of type {:?}",
                                                                ty1.intersection(ty2).collect::<Vec<&Facet>>())
                                            });
                                        }
                                        Ok(Type::new_derived().append(ty1).append(ty2))
                                    },
                                    _ => {
                                        return Err(TError {
                                            reason: "Attempting to call @ on non derived types".to_string()
                                        });
                                    }
                                }
                            },
                            SpecialForm::Drop(dfacet) => {
                                // We get the object from the environment. Dirty solution in place of
                                // proper self/Self methods.
                                let mut obj = env.borrow().lookup_value(&".self".to_string()).unwrap();
                                if let Type::Derived(ref mut fs) = obj {
                                    fs.remove(dfacet.as_str());
                                } else {
                                    panic!("Calling drop on non derived type!");
                                }
                                return Ok(obj);
                            },
                        }
                    },
                    _ => {
                        return Err(TError {
                            reason: format!("Expected function, got: {}", op_type)
                        });
                    }
                }
            }
            Expression::Value(v) => {
                match v {
                    Value::Function(fun) => {
                        let fun_typ = Type::new_function(fun.sig.clone());
                        let scope = Environment::from_parent(env);
                        let Function{sig, arg_names, body} = fun;
                        // Bind the function because we have to typecheck all the nested return
                        // statements. When we encounter one, we resolve "." and we get the function.
                        scope.borrow_mut().bind_value(Identifier::from("."), &fun_typ);
                        // Bind all the generic type parameters.
                        for (id, ty) in &sig.generics {
                            // We also have to check that the types inside are well-defined (aka
                            // every facet is defined).
                            is_type_defined(ty, &scope)?;
                            // And that it is not primitive.
                            if matches!(ty, Type::Primitive(_)) {
                                return Err(TError {
                                    reason: format!("Generics cannot contain primitive type: {}", ty)
                                })
                            }
                            scope.borrow_mut().bind_value(id.to_owned(), ty);
                        }
                        for (name, ty) in arg_names.iter().zip(&sig.args) {
                            // We also have to type_check that the type is well-defined (see above).
                            is_type_defined(ty, &scope)?;
                            // If the type is derived, we attempt to add all the generic parameters
                            // for typechecking. For example <T: A+B>(a: T) we would treat a as a
                            // derived type with T+A+B.
                            if let Type::Derived(fs) = ty {
                                let mut new_ty = ty.clone();
                                for f in fs {
                                    // If it was a generic, we get back the generic and add all its facet.
                                    // Else, we leave it as is.
                                    let gen = scope.borrow().lookup_value(f);
                                    if let Some(Type::Derived(ref more_fs)) = gen {
                                        new_ty = new_ty.append(more_fs);
                                    }
                                }
                                scope.borrow_mut().bind_value(name.to_owned(), &new_ty);
                            } else {
                                scope.borrow_mut().bind_value(name.to_owned(), ty);
                            }
                        }
                        let ret = body.type_check(&scope)?;
                        if !is_subtype(&ret, &sig.ret) {
                            return Err(TError {
                                reason: format!("Return value {} does not match signature {}", ret, sig.ret)
                            });
                        }
                        // Erase the generic parameters. We pretend as if <T: A+B>(a: T) was (a: A+B).
                        let mut new_sig = fun.sig.clone();
                        for i in 0..new_sig.args.len() {
                            let arg = &new_sig.args[i];
                            if let Type::Derived(fs) = arg {
                                let mut new_ty = Type::new_derived();
                                for f in fs {
                                    // If it was a generic, we get back the generic and add all its facet.
                                    // Else, we leave it as is.
                                    let gen = scope.borrow().lookup_value(f);
                                    if let Some(Type::Derived(ref more_fs)) = gen {
                                        new_ty = new_ty.append(more_fs);
                                    } else {
                                        new_ty = new_ty.insert(f);
                                    }
                                }
                                new_sig.args[i] = new_ty;
                            }
                        }
                        // Also erase the generics in the return type.
                        if let Type::Derived(ref fs) = new_sig.ret {
                            let mut new_ret = Type::new_derived();
                            for f in fs {
                                // If it was a generic, we get back the generic and add all its facet.
                                // Else, we leave it as is.
                                let gen = scope.borrow().lookup_value(f);
                                if let Some(Type::Derived(ref more_fs)) = gen {
                                    new_ret = new_ret.append(more_fs);
                                } else {
                                    new_ret = new_ret.insert(f);
                                }
                            }
                            new_sig.ret = new_ret;
                        }
                        // If everything is good, return that it is indeed a function.
                        Ok(Type::new_function_original(new_sig, sig.clone()))
                    }
                    Value::Number(_) => {
                        Ok(TYPE_CORRESPONDENCE.int.clone())
                    }
                    Value::String(_) => {
                        Ok(TYPE_CORRESPONDENCE.str.clone())
                    }
                    Value::Boolean(_) => {
                        Ok(TYPE_CORRESPONDENCE.bool.clone())
                    }
                    Value::Nil => {
                        Ok(TYPE_CORRESPONDENCE.nil.clone())
                    }
                    Value::Identifier(id) => {
                        env.borrow_mut().lookup_field(id)
                    }
                }
            },
            Expression::Block(b) => {
                let block_env = Environment::from_parent(env);
                b.type_check(&block_env)
            }
            Expression::If { condition, body_f, body_t } => {
                if condition.type_check(env)? != TYPE_CORRESPONDENCE.bool {
                    return Err(TError {
                        reason: "If condition has to be a primitive boolean".to_string()
                    });
                }
                let t_bt = body_t.type_check(env)?;
                let t_bf = body_f.type_check(env)?;
                if t_bt == t_bf {
                    Ok(t_bt)
                } else {
                    return Err(TError {
                        reason: "If branches dont have the same type".to_string()
                    });
                }
            }
            Expression::Struct {members, name} => {
                // Get the struct type to see the fields and arity.
                let sty = env.borrow().lookup_value(name).unwrap_or_else(|| panic!("Struct {} not defined", name));
                if let Type::Struct(ref tmembers) = sty {
                    // We have to discount the drop field that is automatically generated.
                    if tmembers.len() - 1 != members.len() {
                        return Err(TError {
                            reason: format!("Missing fields in struct {} initialization", name)
                        });
                    }
                    for (mid, mexp) in members {
                        // Skip the checks for the automatic drop field.
                        if mid == "drop" {
                            continue
                        }
                        let t_mexp = mexp.type_check(env)?;
                        let t_expected = tmembers.get(mid)
                            .ok_or(TError {
                                reason: format!("Field {} not found in struct {}", mid, name)
                            })?;
                        if &t_mexp != t_expected {
                            return Err(TError {
                                reason: format!("Assignment to struct field {}.{} is of wrong type. Got {}, expected {}",
                                                name, mid, t_mexp, t_expected)
                            });
                        }
                    }
                } else {
                    return Err(TError {
                        reason: format!("Expected struct, got {}", name)
                    });
                }
                Ok(Type::new_derived().insert(name))
            }
        }
    }
}

pub fn bind_builtin_type(env: &TEnv, id: Identifier, t: &Type) {
    env.borrow_mut().bind_value(id, t);
}
pub fn type_check(program: &impl TypeCheck) -> Result<(), TError>{
    let tenv = Environment::new();
    for (id, _, sig) in BUILTINS.iter() {
        bind_builtin_type(&tenv, id.clone(), &Type::new_function(sig.clone()));
    }
    for (id, ty) in SPECIAL_FORMS.iter() {
        bind_builtin_type(&tenv, id.clone(), ty);
    }

    program.type_check(&tenv)?;
    Ok(())
}