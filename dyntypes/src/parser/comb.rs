use dyntypes_impl::{alt_n_impl, tuple_n_impl};

use crate::{Information, PError, PResult};
use crate::Parser;
use crate::parser::character::{parse_char, parse_char_cond, parse_literal, whitespace1};

/// Runs the first parser and if it succeeds, returns the result. If it fails, run the second parser.
pub fn legacy_alt<'a, O, F, G>(parser1: F, parser2: G) -> impl Parser<'a, O>
    where F: Parser<'a, O>,
          G: Parser<'a, O>
{
    move |info: Information<'a>| {
        // If parser1 fails, run parser 2
        match parser1.parse(info) {
            o @ Ok(_) => {
                o
            }
            Err(_) => {
                parser2.parse(info)
            }
        }
    }
}

pub trait Tuple<'a, O>: Copy {
    // We cannot return a parser and we have to add the thunk inside tuple function because of
    // non existential types.
    fn fold(&self, info: Information<'a>) -> PResult<'a, O>;
}

pub trait Alt<'a, O>: Copy {
    // We cannot return a parser and we have to add the thunk inside tuple function because of
    // non existential types.
    fn fold(&self, info: Information<'a>) -> PResult<'a, O>;
}

#[tuple_n_impl]
impl<'a, O1, O2, I1, I2> Tuple<'a, (O1, O2)> for (I1, I2)
    where I1: Parser<'a, O1>,
          I2: Parser<'a, O2>
{
    fn fold(&self, info: Information<'a>) -> PResult<'a, (O1, O2)> {
        legacy_tuple(self.0, self.1).parse(info)
    }
}

#[alt_n_impl]
impl<'a, I1, I2, O> Alt<'a, O> for (I1, I2)
    where I1: Parser<'a, O>,
          I2: Parser<'a, O>
{
    fn fold(&self, info: Information<'a>) -> PResult<'a, O> {
        legacy_alt(self.0, self.1).parse(info)
    }
}

pub fn tuple<'a, I, O>(parsers: I) -> impl Parser<'a, O>
    where I: Tuple<'a, O>
{
    move |info: Information<'a>| {
        parsers.fold(info)
    }
}

pub fn alt<'a, I, O>(parsers: I) -> impl Parser<'a, O>
    where I: Alt<'a, O>
{
    move |info: Information<'a>| {
        parsers.fold(info)
    }
}

fn legacy_tuple<'a, O1, O2, F, G>(parser1: F, parser2: G)
                                  -> impl Parser<'a, (O1, O2)>
    where F: Parser<'a, O1>,
          G: Parser<'a, O2>
{
    move |info: Information<'a>| {
        let (i1, o1) = parser1.parse(info)?;
            match parser2.parse(i1) {
                Ok((i2, o2)) => {
                    Ok((i2, (o1, o2)))
                }
                Err(_) => {
                    Err(PError {
                        index: info.index,
                        line:  info.line,
                    })
                }
            }
    }
}

pub fn many0<'a, F, O>(parser: F) -> impl Parser<'a, Vec<O>>
    where F: Parser<'a, O>
{
    move |info: Information<'a>| {
        let mut inp = info;
        let mut vec = Vec::new();
        loop {
            match parser.parse(inp) {
                Ok((i, o)) => {
                    vec.push(o);
                    inp = i;
                }
                Err(_) => {
                    return Ok((inp, vec))
                }
            }
        }
    }
}

pub fn many1<'a, F, O>(parser: F) -> impl Parser<'a, Vec<O>>
    where F: Parser<'a, O>
{
    move |info: Information<'a>| {
        let (i, (o1, mut o2)) = legacy_tuple(parser, many0(parser)).parse(info)?;
        o2.insert(0, o1);
        return Ok((i, o2));
    }
}

pub fn and<'a, O, F, G>(parser1: F, parser2: G) -> impl Parser<'a, O>
    where F: Parser<'a, O>,
          G: Parser<'a, O>,
          O: PartialEq
{
    move |info: Information<'a>| {
        // Only if the have matched the exact same part of the input and they have the same
        // output.
        let (i1, o1) = parser1.parse(info)?;
        let (i2, o2) = parser2.parse(info)?;
        if i1 == i2 && o1 == o2 {
            return Ok((i1, o1))
        } else {
            return Err(PError {
                index: info.index,
                line:  info.line,
            })
        }
    }
}

pub fn optional<'a, O, F>(parser: F) -> impl Parser<'a, Option<O>>
    where F: Parser<'a, O>,
{
    move |info: Information<'a>| {
        match parser.parse(info) {
            Ok((i, o)) => {
                Ok((i, Some(o)))
            }
            Err(_) => {
                Ok((info, None))
            }
        }
    }
}

/// It discards the whitespace around the given parser, left and right. It also discards any line
/// comments.
pub fn junk<'a, F, O>(parser: F) -> impl Parser<'a, O>
    where F: Parser<'a, O>
{
    fn comment(info: Information) -> PResult<()>{
        tuple((parse_literal("//"), many0(parse_char_cond(|c| c != '\n')), parse_char('\n')))
            .parse(info).map(|(i, _)| (i, ()))
    }
    move |info: Information<'a>| {
        let (i, (_, o, _)) = tuple((
            many0(w(comment)),
            w(parser),
            many0(w(comment)),
        )).parse(info)?;
        Ok((i, o))
    }
}

pub fn w<'a, F, O>(parser: F) -> impl Parser<'a, O>
    where F: Parser<'a, O>
{
    move |info: Information<'a>| {
        let (i, (_, o, _)) = tuple((many0(whitespace1), parser, many0(whitespace1))).parse(info)?;
        Ok((i, o))
    }
}

pub fn parse_delimited<'a, F, G, O1, O2>(parse_item: F, parse_deli: G) -> impl Parser<'a, Vec<O1>>
    where F: Parser<'a, O1>,
          G: Parser<'a, O2>
{
    move |info: Information<'a>| {
        let parse_item_comma = move |info: Information<'a>| {
            tuple((parse_item, parse_deli))
                .parse(info)
                .map(|(i, (de, _))| (i, de))
        };
        // Matches: <item><deli><item><deli>...<deli><item>.
        tuple((
            many0(parse_item_comma),
            parse_item,
        )).parse(info).map(|(i, (items_but_last, last_arg))| {
            // Get the identifiers for all but the last arg.
            let mut args: Vec<O1> = items_but_last;
            // Add the last arg.
            args.push(last_arg);
            (i, args)
        })
    }
}
