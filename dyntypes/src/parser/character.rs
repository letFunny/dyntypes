use core::result::Result;
use core::result::Result::{Err, Ok};


use crate::{Information, PError, PResult};
use crate::Parser;

pub fn peek(info: Information) -> Result<char, PError> {
    info.input.get(info.index)
        .ok_or(PError { index: info.index, line: info.line })
        .map(|c| *c)
}

pub fn parse_char_cond<'a, F>(cond: F) -> impl Parser<'a, char>
    where F: Copy + Fn(char) -> bool
{
    move |info: Information<'a>| {
        let c = peek(info)?;
        // Do we have to increment the line count?
        let line_inc = if c == '\n' { 1 } else { 0 };
        if !cond(c) {
            Err(PError {
                index: info.index,
                line:  info.line,
            })
        } else {
            Ok((Information {
                input: info.input,
                index: info.index + 1,
                line:  info.line + line_inc,
            }, c))
        }
    }
}

pub fn parse_char<'a>(l: char) -> impl Parser<'a, char> {
    move |info: Information<'a>| {
        let c = peek(info)?;
        // Do we have to increment the line count?
        let line_inc = if c == '\n' { 1 } else { 0 };
        if c == l {
            Ok((Information{
                input: info.input,
                index: info.index + 1,
                line:  info.line + line_inc,
            }, c))
        } else {
            Err(PError {
                index: info.index,
                line:  info.line,
            })
        }
    }
}

pub fn parse_literal<'a>(literal: &'static str) -> impl Parser<'a, Vec<char>> {
    move |info: Information<'a>| {
        let mut i = info;
        let mut o = Vec::new();
        for c in literal.chars() {
            let (loop_i, loop_o) = parse_char(c).parse(i)?;
            o.push(loop_o);
            i = loop_i;
        }
        return Ok((i, o));
    }
}

pub fn whitespace1(info: Information) -> PResult<char>
{
    let c = peek(info)?;
    if c.is_whitespace() {
        let line_inc = if c == '\n' { 1 } else { 0 };
        Ok((Information {
            input: info.input,
            index: info.index + 1,
            line:  info.line + line_inc,
        }, c))
    } else {
        Err(PError {
            index: info.index,
            line:  info.line,
        })
    }
}
