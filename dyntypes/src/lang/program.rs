use crate::{Parser, PResult};
use crate::Information;
use crate::lang::{Block, Expression, stmt, Value};
use crate::parser::comb::{many1};

pub fn parse_program(info: Information) -> PResult<Expression> {
    // Series of statements that produces no value. It is syntactic sugar for a block where we omit
    // the expression and the opening and closing brackets.
    let (i, stmts) = many1(stmt::parse_statement).parse(info)?;
    Ok((i, Expression::Block(Box::new(Block {
        stmts: stmts,
        expr: Expression::Value(Value::Nil),
    }))))
}

