use std::collections::{BTreeMap, BTreeSet};
use std::fmt::Formatter;
use std::rc::Rc;

pub use expr::parse_expression;
pub use primary::parse_primary;
pub use stmt::parse_statement;
pub use program::parse_program;
pub use graph::graph;



mod expr;
mod primary;
mod stmt;
mod graph;
mod program;
mod typ;

/// Expressions are the statements that produce a value.
#[derive(Debug, Clone)]
pub enum Expression {
    // TODO special form for macros and logic expressions such as and, or.
    FunctionCall {
        op: Box<Expression>,
        children: Vec<Expression>,
    },
    Value(Value),
    Block(Box<Block>),
    If {
        condition: Box<Expression>,
        body_t: Box<Block>,
        body_f: Box<Block>,
    },
    Struct {
        name: Identifier,
        members: Vec<(Identifier, Expression)>,
    },
}

#[derive(Debug, Clone)]
pub struct Block {
    pub stmts: Vec<Statement>,
    pub expr: Expression,
}

#[derive(Debug, Clone)]
pub enum Statement {
    Expression(Expression),
    While {
        condition: Box<Expression>,
        body: Box<Block>,
    },
    Binding {
        var: String,
        var_type: Option<Type>,
        value: Box<Expression>,
    },
    Assignment {
        var: String,
        value: Box<Expression>,
    },
    Return(Expression),
    StructDecl {
        name: Identifier,
        members: Vec<(Identifier, Type)>,
    },
}

// TODO Refactor to use Intern
pub type Identifier = String;
pub type Facet = String;

#[derive(Clone, Debug, Hash, PartialEq, Eq)]
pub enum SpecialForm {
    At,
    Drop(Identifier),
}

#[derive(Clone, Debug, Hash, Eq)]
pub enum Type {
    Primitive(Facet),
    Derived(BTreeSet<Facet>),
    // We keep the type per se and the original one for error reporting in case
    // we performed type erasure.
    Function(Box<Signature>, Option<Box<Signature>>),
    // TreeMap so that we can derive Hash on it for Interning
    Struct(BTreeMap<Identifier, Type>),
    // Identifier and extra args
    SpecialForm(SpecialForm),
}

impl PartialEq for Type {
    fn eq(&self, other: &Self) -> bool {
        // Equality is straightforward for all types except function where we compare the original
        // signatures due to type erasure possibly changing them.
        match (self, other) {
            (Type::Primitive(t1), Type::Primitive(t2)) => t1 == t2,
            (Type::Derived(t1), Type::Derived(t2)) => t1 == t2,
            (Type::Struct(t1), Type::Struct(t2)) => t1 == t2,
            (Type::SpecialForm(s1), Type::SpecialForm(s2)) => s1 == s2,
            (Type::Function(t1, o1), Type::Function(t2, o2)) => {
                // Compare old in case it is set.
                o1.as_ref().unwrap_or(t1) == o2.as_ref().unwrap_or(t2)
            },
            (_, _) => false
        }
    }
}
impl Type {
    pub fn new_primitive(f: impl Into<Facet>) -> Self {
        Type::Primitive(f.into())
    }
    pub fn new_struct(fs: BTreeMap<Identifier, Type>) -> Self {
        Type::Struct(fs)
    }
    pub fn new_function(s: Signature) -> Self {
        Type::Function(Box::new(s), None)
    }
    pub fn new_function_original(s: Signature, org: Signature) -> Self {
        Type::Function(Box::new(s), Some(Box::new(org)))
    }
    pub fn new_derived() -> Self {
        Type::Derived(BTreeSet::new())
    }
    pub fn new_special_form(s: SpecialForm) -> Self {
        Type::SpecialForm(s)
    }
    pub fn insert(self, f: impl Into<Facet>) -> Self {
        let mut new = self.clone();
        if let Type::Derived(ref mut d) = new {
            d.insert(f.into());
            new
        } else {
            panic!("Attempting to add a facet to a non-derived type.")
        }
    }
    pub fn append(self, fs: &BTreeSet<Facet>) -> Self {
        let mut new = self.clone();
        if let Type::Derived(ref mut d) = new {
            d.append(&mut fs.clone());
            new
        } else {
            panic!("Attempting to add a facet to a non-derived type.")
        }
    }
}

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Type::Struct(fs) => {
                write!(f, "<facet: {:?}>", fs)
            }
            Type::Primitive(t) => {
                write!(f, "{}", t)
            }
            Type::Derived(vt) => {
                write!(f, "{}", vt.iter().map(|s| s.to_owned()).collect::<Vec<String>>().join("+"))
            }
            Type::Function(_, original) => {
                // TODO rewrite this
                write!(f, "<function {:?}>", original)
            }
            Type::SpecialForm(s) => {
                write!(f, "<special form {:?}>", s)
            }
        }
    }
}

#[derive(Debug, Clone, PartialEq, Hash, Eq)]
pub struct Signature {
    pub args: Vec<Type>,
    pub generics: Vec<(Identifier, Type)>,
    pub ret: Type,
}

#[derive(Debug, Clone)]
pub struct Function {
    pub sig: Signature,
    pub arg_names: Vec<Identifier>,
    pub body: Box<Block>,
}

#[derive(Debug, Clone)]
pub enum Value {
    Function(Function),
    Number(i64),
    String(String),
    Identifier(Identifier),
    Boolean(bool),
    // We need null because we are not going to implement Option<T>.
    Nil,
}

// Create a different struct for Environment, sort of a tree. We store environments in the hashmap
// along variables value. We pop the environment and append it to the chain we are carrying passing
// it to the next function down the stack and forgetting it when returning to our parent function.

// Environment stores the signatures for functions that allows us to type check.

//   - How to define functions? , proper syntax, or, let IDENTIFIER: TYPE = EXPRESSION where
//     type is (ARG, ARG, ARG) -> RETURN_VALUE
//   - Do we need tuples for this last one? Two ways, either the function type means that or calling
//     functions with several arguments is syntactic sugar for calling with one argument. I slightly
//     prefer the first option.
