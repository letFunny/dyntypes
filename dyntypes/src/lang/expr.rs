use crate::{Information, Parser, PResult};
use crate::lang::{Block, Expression, Function, Identifier, parse_primary, Signature, stmt, typ, Type, Value};
use crate::lang::primary::{parse_identifier, parse_identifier_dot_expr};
use crate::parser::character::{parse_char, parse_literal};
use crate::parser::comb;
use crate::parser::comb::{alt, junk, many0, optional, parse_delimited, tuple};
use crate::typecheck::TYPE_CORRESPONDENCE;

// We cannot do | general parsers because of LL recursion. That is why in each parser we try
// to parse the type and, if not, we go down the ladder. Incidentally, we dont have a system of
// operation precedence because it works thanks to this aforementioned ladder design.

// Another rule, every parser level consumes as much whitespace as possible.
// TODO when parsing something, if you put the binexp before the rest, it calls parse the expression
// a lot of times. Basically <parse expression> check if follows a +, NO -> <parse expression> check
// for *, rinse and repeat. TODO condense in one. Problem: we have to keep precedence.
pub fn parse_expression(info: Information) -> PResult<Expression> {
    junk(alt((parse_function, parse_block, parse_if, parse_tag))).parse(info)
}

pub fn parse_function(info: Information) -> PResult<Expression> {
    // parameter : type
    fn parse_declaration(info: Information) -> PResult<(Identifier, Type)> {
        tuple((
            junk(parse_identifier),
            parse_char(':'),
            junk(typ::parse_type),
        )).parse(info).map(|(i, (id, _, ty))| {
            (i, (id, ty))
        })
    }
    // In generics we allow type variables to not have any facets. Example: <T: F, G> G does not have
    // any facet, it is unconstrained.
    fn parse_empty_declaration(info: Information) -> PResult<(Identifier, Type)> {
        junk(parse_identifier).parse(info)
            .map(|(i, id)| (i, (id, Type::new_derived())))
    }
    // Example: <T: F+G, H>
    fn parse_generics(info: Information) -> PResult<Vec<(Identifier, Type)>> {
        tuple((
            junk(parse_char('<')),
            parse_delimited(alt((parse_declaration, parse_empty_declaration)), parse_char(',')),
            junk(parse_char('>')),
        )).parse(info).map(|(i, (_, decls, _))| (i, decls))
    }
    fn parse_ret(info: Information) -> PResult<Type> {
        tuple((
            junk(parse_literal("->")),
            typ::parse_type,
        )).parse(info).map(|(i, (_, ty))| {
            (i, ty)
        })
    }

    // Matches: ( declaration, declaration, ... , declaration ).
    let arg_list = tuple((
        junk(parse_char('(')),
        optional(comb::parse_delimited(parse_declaration, parse_char(','))),
        junk(parse_char(')'))));

    if let (i, (_, generics, (_, args_opt, _), ret, Expression::Block(block))) = tuple((
        parse_literal("function"),
        optional(parse_generics),
        arg_list,
        junk(optional(parse_ret)),
        parse_block)).parse(info)? {

        let args = args_opt.unwrap_or_else(|| Vec::with_capacity(0));
        let mut arg_types = Vec::with_capacity(args.len());
        let mut arg_names = Vec::with_capacity(args.len());
        for (n, t) in args {
            arg_names.push(n);
            arg_types.push(t);
        }

        return Ok((i, Expression::Value(Value::Function(Function {
            sig: Signature {
                args: arg_types,
                ret: ret.unwrap_or_else(|| TYPE_CORRESPONDENCE.nil.clone()),
                generics: generics.unwrap_or_else(|| Vec::with_capacity(0))
            },
            arg_names: arg_names,
            body: block,
        }))));
    }
    else {
        // This means the parse_block returned an expression that was not a block.
        panic!("We should not reach this point!")
    }

}

pub fn parse_if(info: Information) -> PResult<Expression> {
    let (i, (_, if_cond, if_block, else_opt)) = tuple((
        junk(parse_literal("if")),
        parse_expression,
        parse_block,
        optional(tuple((
            junk(parse_literal("else")),
            parse_block)))
    )).parse(info)?;
    let body_t = match if_block {
        Expression::Block(b) => b,
        _ => panic!("We should not reach this point!")
    };
    // We try to get the block inside the else, if not we create one that evaluates to Nil.
    let body_f = else_opt.map(|t| {
        match t.1 {
            Expression::Block(b) => b,
            _ => panic!("We should not reach this point!")
        }
    }).unwrap_or_else(|| Box::new(Block {
        stmts: Vec::new(),
        expr: Expression::Value(Value::Nil),
    }));

    Ok((i, Expression::If {
        condition: Box::new(if_cond),
        body_t: body_t,
        body_f: body_f,
    }))
}

pub fn parse_block(info: Information) -> PResult<Expression> {
    // A block is a series of statements that ends with an optional expression that produces a value.
    // If no expression if found, the block returns nil.
    let (i, (_, stmts, expr, _)) = tuple((
        junk(parse_char('{')),
        many0(stmt::parse_statement),
        optional(parse_expression),
        junk(parse_char('}')))).parse(info)?;
    Ok((i, Expression::Block(Box::new(Block {
        stmts: stmts,
        expr: expr.unwrap_or(Expression::Value(Value::Nil)),
    }))))
}

pub fn parse_grouping(info: Information) -> PResult<Expression> {
    let (i, (_, e, _)) = tuple((parse_char('('), parse_expression, parse_char(')'))).parse(info)?;
    return Ok((i, e))
}

fn parse_binexp_helper<'a, F, G>(info: Information<'a>, ops: F, ladder: G) -> PResult<Expression>
    where F: Parser<'a, Vec<char>>,
          G: Parser<'a, Expression>
{
    match tuple((junk(ladder), many0(tuple((ops, junk(ladder)))))).parse(info) {
        Ok((i, (exp1, ops))) => {
            let mut left = exp1;
            // If we dont have operations, return the raw expression. If we have them, left associativity.
            if !ops.is_empty() {
                for (op, exp) in ops {
                    left = Expression::FunctionCall {
                        op: Box::new(Expression::Value(Value::Identifier(op.into_iter().collect()))),
                        children: vec![left, exp]
                    };
                }
            }
            Ok((i, left))
        },
        Err(e) => {
            Err(e)
        }
    }
}

pub fn parse_tag(info: Information) -> PResult<Expression> {
    let ops = parse_literal("@");
    parse_binexp_helper(info, ops, parse_logic_or)
}

pub fn parse_logic_or(info: Information) -> PResult<Expression> {
    let ops = parse_literal("or");
    parse_binexp_helper(info, ops, parse_logic_and)
}

pub fn parse_logic_and(info: Information) -> PResult<Expression> {
    let ops = parse_literal("and");
    parse_binexp_helper(info, ops, parse_equality)
}

pub fn parse_equality(info: Information) -> PResult<Expression> {
    let ops = alt((
        parse_literal("=="),
        parse_literal("!=")));
    parse_binexp_helper(info, ops, parse_comparison)
}

pub fn parse_comparison(info: Information) -> PResult<Expression> {
    // Take into account that the operation with two letters should appear first because, else
    // we are going to match the others and return a syntax error.
    let ops = alt((
        parse_literal("<="),
        parse_literal(">="),
        parse_literal("<"),
        parse_literal(">")));
    parse_binexp_helper(info, ops, parse_term)
}

pub fn parse_term(info: Information) -> PResult<Expression> {
    let ops = alt((
        parse_literal("+"),
        parse_literal("-")));
    parse_binexp_helper(info, ops, parse_factor)
}

pub fn parse_factor(info: Information) -> PResult<Expression> {
    let ops = alt((
        parse_literal("*"),
        parse_literal("/")));
    parse_binexp_helper(info, ops, parse_unary)
}

pub fn parse_unary(info: Information) -> PResult<Expression> {
    // We have the luxury of unary not being left recursive.
    match tuple((junk(parse_char('!')), parse_unary)).parse(info) {
        Ok((i, (_, exp))) => {
            Ok((i, Expression::FunctionCall {
                op: Box::new(Expression::Value(Value::Identifier(String::from("!")))),
                children: vec![exp],
            }))
        }
        Err(_) => {
            parse_call.parse(info)
        }
    }
}

pub fn parse_call(info: Information) -> PResult<Expression> {
    // println!("{}", ::tracing::level_filters::LevelFilter::current());
    // For the name, we match an identifier or a expression (the language is functional so we could
    // return a function as a result of an operation).
    // Matches: <name>( expression, expression, ... , expression ).
    // TODO this lets function calls like (arg1, arg2, )
    // Return FunctionCall or go down the Ladder to primary.
    fn parse_arg_comma(info: Information) -> PResult<Expression> {
        tuple((parse_expression, junk(parse_char(',')))).parse(info).map(|(i, (e, _))| (i, e))
    }
    match tuple((
        junk(alt((parse_identifier_dot_expr, parse_grouping))),
        parse_char('('),
        optional(tuple((
            many0(parse_arg_comma),
            parse_expression))
        ),
        junk(parse_char(')')))).parse(info) {
        Ok((i, (fun, _, args_opt, _))) => {
            let mut args_exp: Vec<Expression> = Vec::new();
            if let Some((mut args, exp)) = args_opt {
                args_exp.append(&mut args);
                args_exp.push(exp);
            }
            Ok((i, Expression::FunctionCall {
                op: Box::new(fun),
                children: args_exp,
            }))
        },
        Err(_) => {
            parse_facet_declaration.parse(info)
        }
    }
}

fn parse_facet_declaration(info: Information) -> PResult<Expression> {
    fn parse_facet_member(info: Information) -> PResult<(Identifier, Expression)> {
        tuple((
            junk(parse_identifier),
            parse_char(':'),
            parse_expression,
        )).parse(info).map(|(i, (id, _, e))| (i, (id, e)))
    }
    // Struct member with a trailing comma
    fn parse_facet_member_comma(info: Information) -> PResult<(Identifier, Expression)> {
        tuple((parse_facet_member, junk(parse_char(',')))).parse(info)
            .map(|(i, (s, _))| (i, s))
    }
    match tuple((
        junk(parse_identifier),
        parse_char('{'),
        many0(parse_facet_member_comma),
        optional(tuple((parse_facet_member, optional(junk(parse_char(',')))))),
        junk(parse_char('}'))
    )).parse(info) {
        Ok((i, (name, _, mut members, last_member, _))) => {
            if let Some((m, _)) = last_member {
                members.push(m);
            }

            Ok((i, Expression::Struct {
                name: name,
                members: members,
            }))
        },
        Err(_) => {
            parse_primary.parse(info)
        }
    }
}
