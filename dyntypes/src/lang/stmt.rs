use crate::{Information, lang, Parser, PError, PResult, RESERVED_KEYWORDS};
use crate::lang::{Expression, Identifier, parse_expression, Statement, typ, Type, Value};
use crate::lang::expr::parse_block;
use crate::lang::primary::{parse_identifier, parse_identifier_dot};
use crate::parser::character::{parse_char, parse_char_cond, parse_literal};
use crate::parser::comb::{alt, junk, many0, optional, tuple};

pub fn parse_statement(info: Information) -> PResult<Statement> {
    junk(alt((parse_var_decl, parse_assignment, parse_while, parse_return, parse_facet_decl,
         parse_expression_stmt))).parse(info)
}

/// An expression ended by ; is an statement that no longer produces any value.
fn parse_expression_stmt(info: Information) -> PResult<Statement> {
    let (i, (e, _)) = tuple((lang::parse_expression, junk(parse_char(';')))).parse(info)?;
    Ok((i, Statement::Expression(e)))
}

fn parse_var_decl(info: Information) -> PResult<Statement> {
    fn parse_type_declaration(info: Information) -> PResult<Type> {
        tuple((
            junk(parse_char(':')),
            junk(typ::parse_type),
        )).parse(info).map(|(i, (_, t))| (i, t))
    }
    fn parse_value(info: Information) -> PResult<Expression> {
        tuple((
            junk(parse_char('=')),
            parse_expression,
            )).parse(info).map(|(i, (_, exp))| (i, exp))
    }
    let (i, (_, var, opt_type, val_opt, _)) = tuple((
        junk(parse_literal("let")),
        junk(parse_identifier),
        optional(parse_type_declaration),
        optional(parse_value),
        junk(parse_char(';'))
        )).parse(info)?;

    Ok((i, Statement::Binding {
        var: var,
        var_type: opt_type,
        value: Box::new(val_opt.unwrap_or(Expression::Value(Value::Nil)))
    }))
}

fn parse_while(info: Information) -> PResult<Statement> {
    let (i, (_, cond, block)) = tuple((
        junk(parse_literal("while")),
        parse_expression,
        parse_block
    )).parse(info)?;
    let body = match block {
        Expression::Block(b) => b,
        _ => panic!("We should not reach this point!")
    };
    Ok((i, Statement::While {
        condition: Box::new(cond),
        body: body,
    }))
}

fn parse_assignment(info: Information) -> PResult<Statement> {
    let (i, (var, _, val, _)) = tuple((
        junk(parse_identifier_dot),
        junk(parse_char_cond(|c| c == '=')),
        parse_expression,
        junk(parse_char(';'))
    )).parse(info)?;

    Ok((i, Statement::Assignment {
        var: var,
        value: Box::new(val),
    }))
}

fn parse_return(info: Information) -> PResult<Statement> {
    let (i, (_, e, _)) = tuple((
        junk(parse_literal("return")),
        optional(parse_expression),
        junk(parse_char(';'))
    )).parse(info)?;

    // If no value was provided we still use return for control flow and return nil or empty.
    if let Some(exp) = e {
        Ok((i, Statement::Return(exp)))
    } else {
        Ok((i, Statement::Return(Expression::Value(Value::Nil))))
    }
}

fn parse_facet_decl(info: Information) -> PResult<Statement> {
    fn parse_facet_member(info: Information) -> PResult<(Identifier, Type)> {
        tuple((
            junk(parse_identifier),
            parse_char(':'),
            junk(typ::parse_type),
        )).parse(info).map(|(i, (id, _, ty))| (i, (id, ty)))
    }
    // Struct member with a trailing comma
    fn parse_facet_member_comma(info: Information) -> PResult<(Identifier, Type)> {
        tuple((parse_facet_member, junk(parse_char(',')))).parse(info)
            .map(|(i, (s, _))| (i, s))
    }
    let (i, (_, name, _, mut members, last_member, _)) = tuple((
        junk(parse_literal("facet")),
        parse_identifier,
        junk(parse_char('{')),
        many0(parse_facet_member_comma),
        optional(tuple((parse_facet_member, optional(junk(parse_char(',')))))),
        junk(parse_char('}'))
    )).parse(info)?;
    if let Some((m, _)) = last_member {
        if RESERVED_KEYWORDS.contains(&m.0.as_str()) || m.0 == "drop" {
            return Err(PError {
                index: info.index,
                line:  info.line,
            });
        }
        members.push(m);
    }

    Ok((i, Statement::StructDecl {
        name: name,
        members: members,
    }))
}
