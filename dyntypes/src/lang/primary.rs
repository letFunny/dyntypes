use crate::{Information, Parser, PError, PResult, RESERVED_KEYWORDS};
use crate::parser::character::{parse_char, parse_char_cond, parse_literal};
use crate::parser::comb::{alt, many0, many1, optional, tuple, junk};
use crate::lang::expr::parse_grouping;
use crate::lang::{Expression, Identifier, Value};

type RetType<'a> = PResult<'a, Expression>;

pub fn parse_true(info: Information) -> RetType {
    parse_literal("true").parse(info)
        .map(|(i, _)| (i, Expression::Value(Value::Boolean(true))))
}

pub fn parse_false(info: Information) -> RetType {
    parse_literal("false").parse(info)
        .map(|(i, _)| (i, Expression::Value(Value::Boolean(false))))
}

pub fn parse_nil(info: Information) -> RetType {
    parse_literal("nil").parse(info)
        .map(|(i, _)| (i, Expression::Value(Value::Nil)))
}

pub fn parse_number(info: Information) -> RetType {
    let (i, (minus, digits)) = tuple((
        junk(optional(parse_char_cond(|c| c == '-'))),
        many1(parse_char_cond(|c: char| c.is_digit(10))))).parse(info)?;
    // Convert the vec of digits to a number. Note: we don't have to error check the conversion to
    // a digit because we have already asked for a digit in "digit1".
    let n = digits.iter().map(|c| c.to_digit(10).unwrap()).fold(0, |acc, el| acc * 10 + el);
    if minus.is_some() {
        Ok((i, Expression::Value(Value::Number(-i64::from(n)))))
    } else {
        Ok((i, Expression::Value(Value::Number(i64::from(n)))))
    }
}

pub fn parse_string(info: Information) -> RetType {
    let valid_string_char = parse_char_cond(|c| c != '"');
    let (i, (_, s,_)) = tuple((parse_literal("\""), many0(valid_string_char), parse_literal("\""))).parse(info)?;
    Ok((i, Expression::Value(Value::String(s.into_iter().collect()))))
}
pub fn parse_identifier(info: Information) -> PResult<Identifier> {
    let alphabetic1 = parse_char_cond(|c| c.is_alphabetic() || c == '_');
    let digit1 = parse_char_cond(|c| c.is_digit(10));
    let (i, (s0, s)) = tuple((alphabetic1, many0(alt((alphabetic1, digit1))))).parse(info)?;
    let s: String = std::iter::once(s0).chain(s.into_iter()).collect();
    if RESERVED_KEYWORDS.contains(&&*s) {
        Err(PError {
            index: info.index,
            line:  info.line,
        })
    } else {
        Ok((i, s))
    }
}
pub fn parse_identifier_dot(info: Information) -> PResult<Identifier> {
    fn parse_turbo(info: Information) -> PResult<Identifier> {
        tuple((
            junk(parse_identifier),
            parse_literal("::"),
            junk(parse_identifier),
        )).parse(info)
            .map(|(i, (id1, _l1, id2))| (i, id1 + "::" + &id2))
    }
    fn field_access(info: Information) -> PResult<Identifier> {
        tuple((
            tuple((junk(parse_identifier), junk(parse_char('.')))),
            many0(tuple((
                alt((parse_turbo, junk(parse_identifier))),
                optional(junk(parse_char('.')))
            ))),
        )).parse(info).map(|(i, ((id, _), ids))| {
            let mut ids = ids.into_iter().map(|(id, _)| id).collect::<Vec<Identifier>>();
            ids.insert(0, id);
            (i, ids.join("."))
        })
    }

    alt((field_access, junk(parse_identifier))).parse(info)
}
pub fn parse_identifier_dot_expr(info: Information) -> RetType {
    let (i, id) = parse_identifier_dot.parse(info)?;

    Ok((i, Expression::Value(Value::Identifier(id))))
}

pub fn parse_primary(info: Information) -> RetType {
    junk(alt((parse_true, parse_false, parse_nil, parse_number, parse_string, parse_identifier_dot_expr, parse_grouping))).parse(info)
}
