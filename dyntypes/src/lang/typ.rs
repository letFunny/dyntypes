use crate::{BUILTIN_TYPES, Information, Parser, PError, PResult};
use crate::lang::{Identifier, Signature, Type};
use crate::lang::primary::parse_identifier;
use crate::parser::character::{parse_char, parse_literal};
use crate::parser::comb;
use crate::parser::comb::{alt, junk, optional, parse_delimited, tuple};
use crate::typecheck::TYPE_CORRESPONDENCE;

pub fn parse_type(info: Information) -> PResult<Type> {
    alt((parse_function_type, parse_derived_type)).parse(info)
}

pub fn parse_function_type(info: Information) -> PResult<Type> {
    // parameter : type
    fn parse_declaration(info: Information) -> PResult<(Identifier, Type)> {
        tuple((
            junk(parse_identifier),
            parse_char(':'),
            junk(parse_type),
        )).parse(info).map(|(i, (id, _, ty))| {
            (i, (id, ty))
        })
    }
    fn parse_ret(info: Information) -> PResult<Type> {
        tuple((
            junk(parse_literal("->")),
            parse_type,
        )).parse(info).map(|(i, (_, ty))| {
            (i, ty)
        })
    }
    // In generics we allow type variables to not have any facets. Example: <T: F, G> G does not have
    // any facet, it is unconstrained.
    fn parse_empty_declaration(info: Information) -> PResult<(Identifier, Type)> {
        junk(parse_identifier).parse(info)
            .map(|(i, id)| (i, (id, Type::new_derived())))
    }
    // Example: <T: F+G, H>
    fn parse_generics(info: Information) -> PResult<Vec<(Identifier, Type)>> {
        tuple((
            junk(parse_char('<')),
            parse_delimited(alt((parse_declaration, parse_empty_declaration)), parse_char(',')),
            junk(parse_char('>')),
        )).parse(info).map(|(i, (_, decls, _))| (i, decls))
    }
    let parse_type_list = tuple((
        junk(parse_char('(')),
        optional(comb::parse_delimited(parse_type, parse_char(','))),
        junk(parse_char(')'))));
    tuple((
        parse_literal("fn"),
        optional(parse_generics),
        parse_type_list,
        optional(parse_ret),
    )).parse(info).map(|(i, (_, gens, (_, args, _), ret))| {
        (i, Type::new_function(Signature {
            args: args.unwrap_or_else(|| Vec::with_capacity(0)),
            generics: gens.unwrap_or_else(|| Vec::with_capacity(0)),
            ret: ret.unwrap_or_else(|| TYPE_CORRESPONDENCE.nil.clone()),
        }))
    })
}

pub fn parse_derived_type(info: Information) -> PResult<Type> {
    let (i, ids) = parse_delimited(junk(parse_identifier), parse_char('+')).parse(info)?;
    if ids.len() == 1 {
        let id = ids.first().unwrap();
        if BUILTIN_TYPES.contains(&id.as_str()) {
            Ok((i, (Type::new_primitive(id))))
        } else {
            Ok((i, Type::new_derived().insert(id)))
        }
    } else {
        let mut ty = Type::new_derived();
        for id in ids {
            if BUILTIN_TYPES.contains(&id.as_str()) {
                return Err(PError {
                    index: i.index,
                    line:  i.line,
                })
            } else {
                ty = ty.insert(id);
            }
        }
        Ok((i, ty))
    }
}
