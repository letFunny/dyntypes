use std::fs::File;
use std::io::Write;

use crate::lang::{Block, Expression, Function, Statement, Value};

pub trait ToDotGraph {
    fn graph(&self, nodes_def: &mut String, edges: &mut String, n: usize) -> usize;
}

pub fn graph(file_name: &str, exp: &impl ToDotGraph) -> std::io::Result<()> {
    let prelude = String::from(
r"digraph ast {
ratio = fill;
node [style=filled];
"
    );
    let ending = String::from("}");
    let mut nodes_def = String::from("");
    let mut edges = String::from("");
    exp.graph(&mut nodes_def, &mut edges, 0);
    let mut fp = File::create(file_name).unwrap();
    fp.write_all(prelude.as_ref())?;
    fp.write_all(nodes_def.as_ref())?;
    fp.write_all(edges.as_ref())?;
    fp.write_all(ending.as_ref())?;
    Ok(())
}

impl ToDotGraph for Value {
    fn graph(&self, nodes_def: &mut String, edges: &mut String, n: usize) -> usize {
        match self {
            Value::Function(Function { sig, body, arg_names }) => {
                // Write the node itself
                let sig_name = format!("{:?}", sig);
                let args_name = format!("{:?}", arg_names);
                nodes_def.push_str(&format!(
                    "N{}[label=\"Function; ({}) {}\", color=\"0.641 0.212 1.000\"]\n",
                    n,
                    sig_name.escape_debug(),
                    args_name.escape_debug()));

                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"body\"]\n", n, n + count + 1));
                count += body.as_ref().graph(nodes_def, edges, n + count + 1);

                count + 1
            }
            v => {
                // We have to escape characters such as " -> \" in the labels.
                let name = format!("{:?}", v);
                nodes_def.push_str(&format!("N{}[label=\"{}\", color=\"0.641 0.212 1.000\"]\n", n, name.escape_debug()));
                1
            }
        }
    }
}

impl ToDotGraph for Expression {
    fn graph(&self, nodes_def: &mut String, edges: &mut String, n: usize) -> usize {
        // TODO for better colors check: https://graphviz.org/doc/info/colors.html#brewer
        match self {
            Expression::FunctionCall{op, children} => {
                // Write the node itself
                nodes_def.push_str(&format!("N{}[label=\"expr:CALL\", color=\"0.408 0.498 1.000\"]\n", n));

                let mut count = 0;
                // Write the operation.
                nodes_def.push_str(&format!("N{}[label=\"op\"]\n", n+count+1));
                // Edge to the operation.
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"op\"]\n", n, n+count+1));
                count += op.graph(nodes_def, edges, n+count+1);

                for child in children {
                    edges.push_str(&format!("N{} -> N{}\n", n, n+count+1));
                    count += child.graph(nodes_def, edges, n+count+1);
                }
                count + 1
            },
            Expression::Value(v) => {
                v.graph(nodes_def, edges, n)
            }
            Expression::Block(v) => {
                v.as_ref().graph(nodes_def, edges, n)
            }
            Expression::If {condition, body_t, body_f} => {
                // Write the node itself
                nodes_def.push_str(&format!("N{}[label=\"expr:IF\", color=\"0.408 0.498 1.000\"]\n", n));

                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"cond\"]\n", n, n+count+1));
                count += condition.as_ref().graph(nodes_def, edges, n+count+1);
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"if_true\"]\n", n, n+count+1));
                count += body_t.as_ref().graph(nodes_def, edges, n+count+1);
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"else\"]\n", n, n+count+1));
                count += body_f.as_ref().graph(nodes_def, edges, n+count+1);

                count + 1
            }
            Expression::Struct { name, members } => {
                let struct_name = format!("{:?}", name);
                // Write the node itself
                nodes_def.push_str(&format!("N{}[label=\"expr:STRUCT {}\", color=\"0.408 0.498 1.000\"]\n",
                                            n, struct_name.escape_debug()));

                let mut count = 0;
                for (id, exp) in members {
                    let id_name = format!("{:?}", id);
                    edges.push_str(&format!("N{} -> N{} [label=\"{}\"]\n", n, n+count+1, id_name.escape_debug()));
                    count += exp.graph(nodes_def, edges, n+count+1);
                }
                count + 1
            }
        }
    }
}

impl ToDotGraph for Statement {
    fn graph(&self, nodes_def: &mut String, edges: &mut String, n: usize) -> usize {
        match self {
            Statement::Expression(expr) => {
                expr.graph(nodes_def, edges, n)
            },
            Statement::Binding {var, var_type, value} => {
                let type_name = format!("{:?}", var_type);
                nodes_def.push_str(&format!(
                    "N{}[label=\"stmt:BINDING, {}:{}\", color=\"0.408 0.498 1.000\"]\n",
                    n,
                    var,
                    type_name.escape_debug()));
                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\"]\n", n, n+count+1));
                count += value.as_ref().graph(nodes_def, edges, n+count+1);
                count + 1
            }
            Statement::While {body, condition} => {
                // Write the node itself
                nodes_def.push_str(&format!("N{}[label=\"expr:WHILE\", color=\"0.408 0.498 1.000\"]\n", n));

                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"cond\"]\n", n, n+count+1));
                count += condition.as_ref().graph(nodes_def, edges, n+count+1);
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"body\"]\n", n, n+count+1));
                count += body.as_ref().graph(nodes_def, edges, n+count+1);

                count + 1
            }
            Statement::Assignment {var, value} => {
                nodes_def.push_str(&format!(
                    "N{}[label=\"stmt:ASSIGNMENT, {}\", color=\"0.408 0.498 1.000\"]\n",
                    n,
                    var));
                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\"]\n", n, n+count+1));
                count += value.graph(nodes_def, edges, n+count+1);
                count + 1
            }
            Statement::StructDecl {members, name} => {
                let members_name = format!("{:?}", members);
                nodes_def.push_str(&format!(
                    "N{}[label=\"stmt:STRUCT DECL, {} => {}\", color=\"0.408 0.498 1.000\"]\n",
                    n,
                    name,
                    members_name.escape_debug()));
                1
            }
            Statement::Return(e) => {
                nodes_def.push_str(&format!(
                    "N{}[label=\"stmt:RETURN\", color=\"0.408 0.498 1.000\"]\n", n));
                let mut count = 0;
                edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\"]\n", n, n+count+1));
                count += e.graph(nodes_def, edges, n+count+1);
                count + 1
            }
        }
    }
}

impl ToDotGraph for Block {
    fn graph(&self, nodes_def: &mut String, edges: &mut String, n: usize) -> usize {
        let Block {stmts, expr} = self;
        // Write the node itself
        nodes_def.push_str(&format!("N{}[label=\"expr:BLOCK\", color=\"0.408 0.498 1.000\"]\n", n));

        let mut count = 0;
        for child in  stmts {
            edges.push_str(&format!("N{} -> N{}\n", n, n+count+1));
            count += child.graph(nodes_def, edges, n+count+1);
        }
        // Edge to the expression of the block.
        edges.push_str(&format!("N{} -> N{} [color=\"#4DA6FF\", label=\"expr\"]\n", n, n+count+1));
        count += expr.graph(nodes_def, edges, n+count+1);

        count + 1
    }
}
