#![allow(clippy::needless_return, clippy::redundant_field_names)]
use std::cell::RefCell;
use std::collections::HashMap;
use std::fmt::Formatter;
use std::rc::Rc;

use crate::lang::Identifier;

pub mod lang;
pub mod parser;
pub mod eval;
pub mod typecheck;

const BUILTIN_TYPES: [&str; 4] = ["int", "bool", "nil", "str"];
const RESERVED_KEYWORDS: [&str; 12] = ["if", "while", "function", "true",
    "false", "nil", "return", "facet", "let", "else", "or", "and"];

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Information<'a>
{
    pub input: &'a [char],
    pub index: usize,
    pub line: usize,
}

impl Information<'_>
{
    pub fn new(chars: &[char]) -> Information
    {
        Information {
            input: chars,
            index: 0,
            line:  1,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PError {
    pub index: usize,
    pub line:  usize,
}

impl std::fmt::Display for PError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "line {}", self.line)
    }
}

pub type PResult<'a, O> = Result<(Information<'a>, O), PError>;

pub trait Parser<'a, O>: Copy {
    fn parse(&self, info: Information<'a>) -> PResult<'a, O>;
}

impl<'a, O, T> Parser<'a, O> for T
    where T: Copy + Fn(Information<'a>) -> PResult<'a, O>
{
    #[inline]
    fn parse(&self, info: Information<'a>) -> PResult<'a, O> {
        self(info)
    }
}

type Env<T> = HashMap<Identifier, T>;

// TODO optimization, also use Cow for environment
#[derive(Debug, Clone)]
pub struct Environment<T>
    where T: Clone
{
    env: Env<T>,
    parent: Option<Rc<RefCell<Environment<T>>>>
}

// TODO override Debug for environment because of infinite loops
impl<T> Environment<T>
    where T: Clone
{
    pub fn lookup_value(&self, id: &Identifier) -> Option<T> {
        let v = self.env.get(id).cloned();
        if v.is_none() && self.parent.is_some() {
            // Search recursively through the parent list.
            self.parent.as_ref().unwrap().borrow().lookup_value(id)
        } else {
            v
        }
    }
    pub fn bind_value(&mut self, id: Identifier, val: &T) {
        self.env.insert(id, val.clone());
    }
    pub fn new() -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Environment {
            env: HashMap::new(),
            parent: None,
        }))
    }
    pub fn from_parent(parent: &Rc<RefCell<Environment<T>>>) -> Rc<RefCell<Self>> {
        Rc::new(RefCell::new(Environment {
            env: HashMap::new(),
            parent: Some(parent.clone()),
        }))
    }
}
