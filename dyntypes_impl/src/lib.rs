
use proc_macro2::{Ident, Span};
use quote::{ToTokens};
use syn::{parse_macro_input, Generics, parse_quote, ItemImpl, ImplItemMethod, ImplItem};


struct Configuration {
    trait_name: &'static str,
    implementation: fn (&mut ImplItemMethod, Vec<syn::punctuated::Punctuated<syn::Expr, syn::token::Comma>>),
    generic_parameters: fn (&mut Generics, Ident, Ident),
    outputs: fn(&mut ItemImpl, &mut ImplItemMethod, Ident, Vec<Ident>),
}

#[proc_macro_attribute]
pub fn tuple_n_impl(_attr: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    fold_function(item, Configuration {
        trait_name: "Tuple",
        implementation: tuple_implementation,
        generic_parameters: tuple_generic_parameters,
        outputs: tuple_outputs,
    })
}

#[proc_macro_attribute]
pub fn alt_n_impl(_attr: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    fold_function(item, Configuration {
        trait_name: "Alt",
        implementation: alt_implementation,
        generic_parameters: alt_generic_parameters,
        outputs: alt_outputs,
    })
}

fn fold_function(item: proc_macro::TokenStream, conf: Configuration) -> proc_macro::TokenStream {
    // The helper trait we are going to implement.
    let trait_ident = Ident::new(conf.trait_name, Span::call_site());

    let input = parse_macro_input!(item as ItemImpl);
    // We are going to add all the implementations to this variable and return that.
    let mut tokens = input.clone().into_token_stream();

    for max in 3usize..10usize {
        let mut new_impl = input.clone();
        // Clear all the existing generics and where clause.
        new_impl.generics.params.clear();
        new_impl.generics.where_clause = None;
        // Add the lifetime back. It has to be added first.
        new_impl.generics.params.push(parse_quote!('a));

        // Ge the function from inside the block.
        let mut fun: ImplItemMethod = new_impl.items.iter_mut().filter_map(
            |i| {
                if let ImplItem::Method(m) = i {
                    Some(m)
                } else {
                    None
                }
            }).next().unwrap().clone();
        // Clear the inputs and add the two parameters: (&self, info: Information<'a>)
        fun.sig.inputs.clear();
        fun.sig.inputs.push(parse_quote!(&self));
        fun.sig.inputs.push(parse_quote!(info: Information<'a>));

        // We are going to store here the input and output types for later use. Example: inputs could
        // hold idents for [I0, I1, I2, I3, I4], and output for [O0, O1, O2, O3, O4].
        let mut inputs = Vec::new();
        let mut outputs = Vec::new();
        // We will store the variables we need latter for the function calls. In this case, for a
        // tuple we need, "self.0", "self.1", "self.2", etc.
        let mut variables: Vec<syn::punctuated::Punctuated<syn::Expr, syn::token::Comma>> = Vec::new();

        for n in 0usize..max {
            // Names for the generic parameters of this iteration.
            let i_type = format!("I{}", n);
            let o_type = format!("O{}", n);
            let input = Ident::new(&i_type, Span::call_site());
            let output = Ident::new(&o_type, Span::call_site());
            let num = syn::Index::from(n);

            outputs.push(output.clone());
            inputs.push(input.clone());
            variables.push(parse_quote!(self.#num));

            (conf.generic_parameters)(&mut new_impl.generics, input, output);
        }
        // We are implementing the trait for the tuple (I0, I1, I2, ..., IN). Note: the index goes
        // to N-1 in reality.
        new_impl.self_ty = parse_quote!((#(#inputs),*));
        (conf.outputs)(&mut new_impl, &mut fun, trait_ident.clone(), outputs);
        (conf.implementation)(&mut fun, variables);

        new_impl.items.clear();
        new_impl.items.push(ImplItem::Method(fun));
        // Add this implementation to the final result.
        tokens.extend(new_impl.into_token_stream());
    }
    proc_macro::TokenStream::from(tokens)
}

fn tuple_implementation(fun: &mut ImplItemMethod, variables: Vec<syn::punctuated::Punctuated<syn::Expr, syn::token::Comma>>) {
    // We perform two calls. The first one we call tuple with all but one variable. Because we
    // are going in order, it is already implemented for n-1 length tuples. We then perform one
    // more call manually to get the last one.
    let all_but_last_variables = variables.iter().take(variables.len() - 1);
    let last_variable = variables.last().unwrap();
    // We have to deconstruct the tuple returned from the first call into "o.1", "o.2", etc. in
    // order to assemble the final result. We store here the variables.
    let o_tuple: Vec<syn::punctuated::Punctuated<syn::Expr, syn::token::Comma>> = (0usize..variables.len()-1).map(
        |n| {
            let num = syn::Index::from(n);
            return parse_quote!(o.#num);
        }).collect();
    // The block, we perform the two calls as described.
    fun.block.stmts = parse_quote!{
            let (i, o) = tuple((#(#all_but_last_variables),*)).parse(info)?;
            let (ii, oo) = #last_variable.parse(i)?;
            return Ok((ii, (#(#o_tuple),* ,oo)));
        };
}

fn tuple_generic_parameters(generics: &mut Generics, input: Ident, output: Ident) {
    generics.params.push(parse_quote!(#output));
    generics.params.push(parse_quote!(#input: Parser<'a, #output>));
}

fn tuple_outputs(new_impl: &mut ItemImpl, fun: &mut ImplItemMethod, trait_ident: Ident, outputs: Vec<Ident>) {
    new_impl.trait_ = Some((None, parse_quote!(#trait_ident<'a, (#(#outputs),*)>), syn::token::For::default()));
    fun.sig.output = parse_quote!(-> PResult<'a, (#(#outputs),*)>);
}

fn tuple_output_trait(new_impl: &mut ItemImpl, fun: &mut ImplItemMethod, trait_ident: Ident, outputs: Vec<Ident>) {
    // The trait itself is "Tuple<'a, (I0, I1, I2, ..., IN)>". Note: the index goes
    // to N-1 in reality.
    new_impl.trait_ = Some((None, parse_quote!(#trait_ident<'a, (#(#outputs),*)>), syn::token::For::default()));
    fun.sig.output = parse_quote!(-> PResult<'a, (#(#outputs),*)>);
}

fn alt_implementation(fun: &mut ImplItemMethod, variables: Vec<syn::punctuated::Punctuated<syn::Expr, syn::token::Comma>>) {
    // We perform two calls. The first one we call alt with all but one variable. Because we
    // are going in order, it is already implemented for n-1 length tuples. We then perform one
    // more call manually to get the last one.
    let all_but_last_variables = variables.iter().take(variables.len() - 1);
    let last_variable = variables.last().unwrap();
    // The block, we perform the two calls as described.
    fun.block.stmts = parse_quote!{
        return alt((alt((#(#all_but_last_variables),*)), #last_variable)).parse(info);
    };
}

fn alt_generic_parameters(generics: &mut Generics, input: Ident, _output: Ident) {
    // If the list has <'a>, we have to push O first before the input generics.
    if generics.params.len() == 1 {
        generics.params.push(parse_quote!(O));
    }
    generics.params.push(parse_quote!(#input: Parser<'a, O>));
}

fn alt_outputs(new_impl: &mut ItemImpl, fun: &mut ImplItemMethod, trait_ident: Ident, _outputs: Vec<Ident>) {
    // Alt<'a, O>
    new_impl.trait_ = Some((None, parse_quote!(#trait_ident<'a, O>), syn::token::For::default()));
    fun.sig.output = parse_quote!(-> PResult<'a, O>);
}
/*fn create_generic_param(signature: &mut Signature, name: &str, output_name: &str) {
    let generics = &mut signature.generics;
    let input = Ident::new(name, Span::call_site());
    let output = Ident::new(output_name, Span::call_site());

    // Add the input parameter
    let inputs = &mut signature.inputs;
    inputs.push(parse_quote!(parser3: #input));

    // Create the generic type variable
    let t = TypeParam::from(input.clone());
    generics.params.push(GenericParam::Type(t));

    // Specify the generic bound
    generics.where_clause.as_mut().unwrap().predicates.push(parse_quote!(#input: Parser<'a, #output>));

    // TODO set default for unwrap
    // println!("Found type: {:?}", t);
    // println!("Found bounds: {:?}", t.bounds)
}*/
/*fn find_param(generics: &Generics) -> Option<&TypeParam> {
    for param in &generics.params {
        if let GenericParam::Type(t) = param {
            if t.ident.to_string() == String::from("F") {
                return Some(t);
            }
        }
    }
    return None;
}*/
