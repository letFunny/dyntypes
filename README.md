# Getting started
## How to compile
Assumes rust and cargo are installed and the nightly branch is selected.
```sh
cargo build
```

## How to run
```sh
cargo run -- my_program.txt
```

## How to run the tests
```sh
python test.py
```

# Language
For the complete language description, rationale and the different design
decisions that went into creating the interpreter please see my bachelor
thesis [here](https://letfunny.com/files/dyntypes.pdf).
