from tqdm import tqdm
from glob import glob
import subprocess
import sys

# For whatever reason the debug relase is painfully slow when dealing with
# this quantity of recursion (parser combinators).
BIN = "target/release/dyntypes"

# Compile the program
print("Compiling the program...")
t = subprocess.run(["cargo", "build", "--release"], capture_output=True)
if t.returncode != 0:
    print("Failed to compile the program, exiting.")
    sys.exit(1)

# Get the name of the tests without the extension
cases = list(map(lambda name: name.split(".in")[0], glob("tests/*.in")))
failed = []
error = 0
for case in tqdm(cases):
    ex = subprocess.run([BIN, case + ".in"], capture_output=True)
    with open(case + ".out", "rb") as fp:
        s = fp.read()
    try:
        lines = s.split(b"\n")
        fields = lines[0].split()
        if len(fields) != 2 or fields[0] != b"//":
            error = 1
            break
        ret_code = int(fields[1])
        expected = b"\n".join(lines[1:])
    except IndexError | ValueError:
        error = 1
        break
    # If ret code is 0, check output, if error just check return code.
    if ex.returncode != ret_code or (ret_code == 0 and ex.stdout != expected):
        failed.append({"case": case, "expected": expected, "actual": ex.stdout,
            "ret_code": ex.returncode, "ret_expected": ret_code})

# Errors in the test files format
if error == 1:
    print("Bad formatting on test case file: {}".format(case + ".out"))
    sys.exit(1)

print("--------------------------------------------------")
print("Error 101: Probably panic.")
print("Error 121: Probably parse error.")
print("--------------------------------------------------")
for f in failed:
    print("Case {}".format(f["case"]))
    print("Return code {}, expected {}".format(f["ret_code"], f["ret_expected"]))
    print(">>>>>>>>>>>")
    print("{}".format(f["expected"]))
    print("<<<<<<<<<<<")
    print("{}".format(f["actual"]))
    print("-----------")

print("Failed {} of {}.".format(len(failed), len(cases)))
